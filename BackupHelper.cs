﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq.Expressions;
using System.Text;

namespace MongodbBackupHelper;

public static class BackupHelper
{
    public static async Task MongoDump(string uri, string outputPath)
    {
        var startInfo = new ProcessStartInfo
        {
            FileName = "mongodump",
            Arguments = $"--uri {uri} --out {outputPath} --gzip",
            CreateNoWindow = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
        };
        var process = new Process { StartInfo = startInfo };
        process.OutputDataReceived += (sender, args) =>
        {
            Console.WriteLine($"Output: {args.Data}");
        };
        process.ErrorDataReceived += (sender, args) =>
        {
            Console.WriteLine($"Output: {args.Data}");
        };
        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        await process.WaitForExitAsync();
    }
    public static async Task ExportMetadata(this IMongoDatabase db, string collectionName, string fileDirectory)
    {
        //get collection with metadata
        var collection = await db.GetCollectionWithMetadata(collectionName);
        //get metadata of collection
        var metaDataModel = collection.GetMetadata();
        //set save file location
        var filePath = SetFilePath(fileDirectory, collectionName, ".metadata.json.gz");
        //write file
        using (var fs = new FileStream(filePath, FileMode.Create))
        using (var gz = new GZipStream(fs, CompressionMode.Compress))
        {
            var jsonString = metaDataModel.ToJson();
            var bytes = Encoding.UTF8.GetBytes(jsonString);
            await gz.WriteAsync(bytes);
        }
    }
    public static async Task ToCompressedBson(this IMongoDatabase db, Expression<Func<BsonDocument, bool>> filter, string collectionName, string fileDirectory)
    {
        //skip if collection is view
        var collection = await db.GetCollectionWithMetadata(collectionName);
        if (collection.TryGetValue("type", out var type))
        {
            if (type == "view") return;
        }
        else throw new Exception($"tipe collection {collectionName} tidak ditemukan");
        //get cursor
        var cursor = await db.GetCollection<BsonDocument>(collectionName).FindAsync(filter);
        //set file path
        var filePath = SetFilePath(fileDirectory, collectionName, ".bson.gz");
        //write file
        var fileMode = File.Exists(filePath) ? FileMode.Append : FileMode.Create;
        using (var fs = new FileStream(filePath, fileMode))
        using (var gz = new GZipStream(fs, CompressionMode.Compress))
        {
            //set data queue
            var bufferQueue = new BlockingCollection<IEnumerable<BsonDocument>>();
            //produce data queue
            var cursorTask = Task.Run(async () =>
            {
                var index = 0;
                while (await cursor.MoveNextAsync())
                {
                    Console.WriteLine("writing " + collectionName + "... " + index++);
                    var datas = cursor.Current;
                    //store data to queue
                    bufferQueue.Add(datas);
                }
                bufferQueue.CompleteAdding();
            });
            //consume data queue
            var writeTask = Task.Run(async () =>
            {
                foreach (var buffer in bufferQueue.GetConsumingEnumerable())
                {
                    foreach (var data in buffer)
                    {
                        //write data into file
                        await gz.WriteAsync(data.ToBson());
                    }
                }
            });
            await cursorTask;
            await writeTask;
        }
    }
    public static async Task RestoreFromCompressedBson(this IMongoCollection<BsonDocument> collection, string fileDirectory, int batch = 100000)
    {
        //set file path
        var filePath = SetFilePath(fileDirectory, collection.CollectionNamespace.CollectionName, ".bson.gz");
        try
        {
            using (var fs = new FileStream(filePath, FileMode.Open))
            using (var gz = new GZipStream(fs, CompressionMode.Decompress))
            using (var fsd = new FileStream(filePath.Replace(".gz", ""), FileMode.Create))
            {
                //extract file
                await gz.CopyToAsync(fsd);
                fsd.Seek(0, SeekOrigin.Begin);
                //set data buffer
                var datas = new List<BsonDocument>(batch);
                var tasks = new List<Task>();
                while (fsd.Position < fsd.Length)
                {
                    //get bson document
                    var data = BsonSerializer.Deserialize<BsonDocument>(fsd);
                    datas.Add(data);
                    if (datas.Count >= batch)
                    {
                        //insert data to collection
                        tasks.Add(collection.TryInsertManyAsync(datas));
                        //reset data buffer
                        datas = new List<BsonDocument>(batch);
                    }
                }
                if (datas.Count > 0)
                {
                    //insert rest of data to collection
                    tasks.Add(collection.TryInsertManyAsync(datas));
                }
                await Task.WhenAll(tasks);
            }
        }
        finally
        {
            var uncompressedFilePath = filePath.Replace(".gz", "");
            if (File.Exists(uncompressedFilePath))
            {
                File.Delete(uncompressedFilePath);
            }
        }
    }
    public static async Task IncrementalBackup(this IMongoClient mongoClient, string backupDirectory, long intervalSeconds = 1)
    {
        var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };
        using (var changeStream = await mongoClient.WatchAsync(options))
        {
            await ProcessChanges(changeStream, backupDirectory, intervalSeconds);
        }
    }
    public static async Task IncrementalBackup(this IMongoDatabase mongoDatabase, string backupDirectory, long intervalSeconds = 1)
    {
        var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };
        using (var changeStream = await mongoDatabase.WatchAsync(options))
        {
            await ProcessChanges(changeStream, backupDirectory, intervalSeconds);
        }
    }
    public static async Task IncrementalBackup(this IMongoCollection<BsonDocument> mongoCollection, string backupDirectory, long intervalSeconds = 1)
    {
        var options = new ChangeStreamOptions { FullDocument = ChangeStreamFullDocumentOption.UpdateLookup };
        using (var changeStream = await mongoCollection.WatchAsync(options))
        {
            await ProcessChanges(changeStream, backupDirectory, intervalSeconds);
        }
    }
    private static async Task<BsonDocument> GetCollectionWithMetadata(this IMongoDatabase db, string collectionName)
    {
        var collectionsOptions = new ListCollectionsOptions { Filter = new BsonDocument("name", collectionName) };
        var collection = await db.ListCollections(collectionsOptions).FirstOrDefaultAsync() ?? throw new Exception("collection name tidak ditemukan!");
        return collection;
    }
    private static async Task ProcessChanges(IChangeStreamCursor<ChangeStreamDocument<BsonDocument>> changeStream, string backupDirectory, long intervalSeconds = 1)
    {
        await changeStream.ForEachAsync(async change =>
        {
            //get name property
            var collectionName = change.CollectionNamespace.CollectionName;
            var dbName = change.DatabaseNamespace.DatabaseName;
            //set datetime name
            var dateTime = change.WallTime ?? DateTime.Now;
            var timestamp = new DateTimeOffset(dateTime).ToUnixTimeSeconds();
            dateTime = dateTime.AddSeconds(-(timestamp % intervalSeconds));
            var dateTimeName = dateTime.ToString("yyyyMMddTHHmmssK");
            //get document
            var bsonDocument = change.FullDocument;
            //set file path
            var fileDirectory = Path.Combine(backupDirectory, dbName);
            var filePath = SetFilePath(fileDirectory, collectionName + "_" + dateTimeName, ".bson.gz");
            //stream file
            var fileMode = FileMode.Create;
            if (File.Exists(filePath)) fileMode = FileMode.Append;
            using (var fs = new FileStream(filePath, fileMode))
            using (var gz = new GZipStream(fs, CompressionMode.Compress))
            {
                Console.WriteLine("data inserted into " + filePath);
                //insert data
                await gz.WriteAsync(bsonDocument.ToBson());
            }
        });
    }
    private static string SetFilePath(string fileDirectory, string fileName, string extension)
    {
        fileName = fileName + extension;
        if (!Directory.Exists(fileDirectory)) Directory.CreateDirectory(fileDirectory);
        var filePath = Path.Combine(fileDirectory, fileName);
        return filePath;
    }
    private static BsonDocument GetMetadata(this BsonDocument collection)
    {
        //get metaData to check
        collection.TryGetValue("name", out var name);
        collection.TryGetValue("type", out var type);
        var metaDataModel = new BsonDocument
        {
                {"collectionName", name },
                {"type", type}
            };
        if (collection.TryGetValue("idIndex", out var indexes))
            metaDataModel.Add("indexes", new BsonArray { indexes });
        if (collection.TryGetValue("info", out var info))
        {
            if (info.AsBsonDocument.TryGetValue("uuid", out var csuuid))
            {
                var uuid = csuuid?.AsGuid;
                metaDataModel.Add("uuid", BsonValue.Create(uuid.ToString()));
            }
        }
        if (collection.TryGetValue("options", out var options))
            metaDataModel.Add("options", options);
        return metaDataModel;
    }
    private static async Task TryInsertManyAsync<T>(this IMongoCollection<T> collection, IEnumerable<T> datas)
    {
        try
        {
            await collection.InsertManyAsync(datas);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}
